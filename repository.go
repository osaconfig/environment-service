// environment-service/repository.go
package main

import (
	envdPb "gitlab.com/osaconfig/protobufs/envd"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	dbName               = "osaconfig"
	userConfigCollection = "environment-overrides"
)

// Repository - interface for user config repository methods
type Repository interface {
	GetEnvironment(*envdPb.GetRequest) (*envdPb.Environment, error)
	CreateEnvironment(*envdPb.Environment) error
	RemoveEnvironment(*envdPb.GetRequest) error
	Close()
}

// EnvironmentRepository - struct to contain datastore session
type EnvironmentRepository struct {
	session *mgo.Session
}

// GetEnvironment - returns an environment override object
func (repo *EnvironmentRepository) GetEnvironment(envd *envdPb.GetRequest) (*envdPb.Environment, error) {
	var environment *envdPb.Environment

	err := repo.collection().Find(bson.M{
		"envd": bson.M{"$eq": envd.Envd},
	}).One(&environment)

	if err != nil {
		return nil, err
	}

	return environment, nil
}

// CreateEnvironment - creates a new environment in the datastore
func (repo *EnvironmentRepository) CreateEnvironment(envd *envdPb.Environment) error {
	return repo.collection().Insert(envd)
}

// RemoveEnvironment - removes an environment from the collection
func (repo *EnvironmentRepository) RemoveEnvironment(envd *envdPb.GetRequest) error {
	return repo.collection().Remove(bson.M{
		"environment": bson.M{"$eq": envd.Envd},
	})
}

//TODO(d34dh0r53): See note in protobuf
/*
func (repo *EnvironmentRepository) UpdateEnvironments(envd *envdPb.UpdateEnvd) (*envdPb.UpdateEnvdResponse, error) {

}
*/

// Close - close the datastore
func (repo *EnvironmentRepository) Close() {
	repo.session.Close()
}

func (repo *EnvironmentRepository) collection() *mgo.Collection {
	return repo.session.DB(dbName).C(userConfigCollection)
}
