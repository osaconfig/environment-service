// environment-service/main.go
package main

import (
	"fmt"
	"log"
	"os"

	micro "github.com/micro/go-micro"
	k8s "github.com/micro/kubernetes/go/micro"
	envdPb "gitlab.com/osaconfig/protobufs/envd"
)

const (
	defaultHost = "localhost:27017"
)

func createDummyData(repo Repository) {
	defer repo.Close()
}

func main() {

	// Database host from environment variables set in Dockerfile or spawning process
	host := os.Getenv("DB_HOST")
	// Fallback on defaultHost
	if host == "" {
		host = defaultHost
	}

	session, err := CreateSession(host)

	defer session.Close()

	if err != nil {
		log.Panicf("Could not connect to datastore with host %s - %v", host, err)
	}

	srv := k8s.NewService(
		micro.Name("osaconfig.environmentservice"),
		micro.Version("latest"),
	)

	//	repo := &EnvironmentRepository{session.Copy()}

	srv.Init()

	// Register the handler
	envdPb.RegisterEnvironmentServiceHandler(srv.Server(), &service{session})

	if err := srv.Run(); err != nil {
		fmt.Println(err)
	}
}
