// environment-service/handler.go
package main

import (
	"context"

	envdPb "gitlab.com/osaconfig/protobufs/envd"
	mgo "gopkg.in/mgo.v2"
)

// Our gRPC service handler
type service struct {
	session *mgo.Session
}

func (s *service) GetRepo() Repository {
	return &EnvironmentRepository{s.session.Clone()}
}

func (s *service) GetEnvironment(ctx context.Context, req *envdPb.GetRequest, res *envdPb.Response) error {
	defer s.GetRepo().Close()
	// Find the environment
	envd, err := s.GetRepo().GetEnvironment(req)
	if err != nil {
		return err
	}

	// Set the inventory as part of the response message type
	res.Envd = envd
	return nil
}

func (s *service) CreateEnvironment(ctx context.Context, req *envdPb.Environment, res *envdPb.Response) error {
	defer s.GetRepo().Close()
	if err := s.GetRepo().CreateEnvironment(req); err != nil {
		return err
	}
	res.Envd = req
	res.Created = true
	return nil
}

//TODO(d34dh0r53): See note in protobuf
/*
func (s *service) UpdateEnvironments(ctx context.Context, req *envdPb.UpdateEnvd, res *envdPb.UpdateEnvdResponse) error {
	defer s.GetRepo().Close()
	if err := s.GetRepo().UpdateEnvironments(req); err != nil {
		return err
	}
	res.Envd =
}
*/

func (s *service) RemoveEnvironment(ctx context.Context, req *envdPb.GetRequest, res *envdPb.Response) error {
	defer s.GetRepo().Close()
	if err := s.GetRepo().RemoveEnvironment(req); err != nil {
		return err
	}
	res.Envd = nil
	res.Removed = true
	return nil
}
